package com.zeelo.zeelobooks.koin

import android.app.Application
import com.zeelo.zeelobooks.BuildConfig
import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import com.zeelo.zeelobooks.features.books.repository.local.BooksLocalRepository
import com.zeelo.zeelobooks.features.books.repository.remote.BooksRemotelRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module


val repositoryModuleList = module {

    single<BooksRepository> { getRepositorySource(androidApplication()) }


}

fun getRepositorySource(application: Application): BooksRepository =

        when (BuildConfig.BUILD_TYPE) {
            "debug", "release" -> {
                BooksRemotelRepository()
            }
            else -> {
                BooksLocalRepository(application)
            }

        }
