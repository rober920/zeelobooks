package com.zeelo.zeelobooks.koin


import com.zeelo.zeelobooks.features.books.createbook.presenter.CreateBookPresenter
import com.zeelo.zeelobooks.features.books.bookdetail.presenter.BookDetailPresenter
import com.zeelo.zeelobooks.features.books.booklist.presenter.BookListPresenter
import org.koin.core.parameter.ParameterDefinition
import org.koin.core.parameter.emptyParameterDefinition
import org.koin.core.scope.Scope
import org.koin.standalone.StandAloneContext


inline fun <reified T : Any> BookDetailPresenter.inject(
        name: String = "",
        scope: Scope? = null,
        noinline parameters: ParameterDefinition = emptyParameterDefinition()
) = lazy { StandAloneContext.getKoin().koinContext.get<T>(name, scope, parameters) }

inline fun <reified T : Any> BookListPresenter.inject(
        name: String = "",
        scope: Scope? = null,
        noinline parameters: ParameterDefinition = emptyParameterDefinition()
) = lazy { StandAloneContext.getKoin().koinContext.get<T>(name, scope, parameters) }


inline fun <reified T : Any> CreateBookPresenter.inject(
        name: String = "",
        scope: Scope? = null,
        noinline parameters: ParameterDefinition = emptyParameterDefinition()
) = lazy { StandAloneContext.getKoin().koinContext.get<T>(name, scope, parameters) }

