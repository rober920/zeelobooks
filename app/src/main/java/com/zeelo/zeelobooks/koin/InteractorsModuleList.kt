package com.zeelo.zeelobooks.koin

import com.zeelo.zeelobooks.features.books.createbook.model.interactors.CreateBookInteractor
import com.zeelo.zeelobooks.features.books.bookdetail.model.interactors.GetBookDetailInteractor
import com.zeelo.zeelobooks.features.books.bookdetail.model.mapper.BookDetailMapper
import com.zeelo.zeelobooks.features.books.booklist.model.interactors.GetBookListInteractor
import com.zeelo.zeelobooks.features.books.booklist.model.mapper.BookListMapper
import org.koin.dsl.module.module

val interactorModuleList = module {

  factory { GetBookListInteractor(get(), BookListMapper()) }
  factory { GetBookDetailInteractor(get(), BookDetailMapper()) }
  factory { CreateBookInteractor(get(), BookDetailMapper()) }

}