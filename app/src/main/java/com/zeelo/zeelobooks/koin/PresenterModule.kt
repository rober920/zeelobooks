package com.zeelo.zeelobooks.koin;

import com.zeelo.zeelobooks.features.books.createbook.presenter.impl.CreateBookPresenterImpl
import com.zeelo.zeelobooks.features.books.bookdetail.presenter.impl.BookDetailPresenterImpl
import com.zeelo.zeelobooks.features.books.booklist.presenter.impl.BookListPresenterImpl
import org.koin.dsl.module.module


val presenterModuleList = module {

    factory { BookListPresenterImpl() }
    factory { BookDetailPresenterImpl() }
    factory { CreateBookPresenterImpl() }

}