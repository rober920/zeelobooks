package com.bet4gamers.app.base

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class ZeeloBaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    companion object {
        var LOADING_ITEM = 0
        var DATA_ITEM = 1

    }

    abstract fun getType(): Int
}