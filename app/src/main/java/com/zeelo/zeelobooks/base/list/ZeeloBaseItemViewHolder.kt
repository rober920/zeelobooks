package com.bet4gamers.app.base

import android.view.View

abstract class ZeeloBaseItemViewHolder<M>(itemView: View) : ZeeloBaseViewHolder(itemView) {


    abstract fun bind(item: M)

    override fun getType(): Int {
        return DATA_ITEM
    }
}