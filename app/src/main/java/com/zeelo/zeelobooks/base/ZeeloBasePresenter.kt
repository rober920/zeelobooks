package com.zeelo.zeelobooks.base

import android.content.Context
import android.support.annotation.CallSuper

abstract class ZeeloBasePresenter<V : ZeeloBasePresenter.BaseView> {

    var mView: V? = null


    protected fun onServiceError(error: Throwable?) {
        mView?.hideLoading()
        mView?.showGenericError()

//    open fun onError(error: ApiError) {
//        mView?.showGenericError()
//    }
    }

    @CallSuper
    open fun onDestroy() {
        mView = null
    }

    interface BaseView {
        fun showLoading()
        fun hideLoading()
        fun showGenericError()
        fun getViewContext(): Context
    }
}