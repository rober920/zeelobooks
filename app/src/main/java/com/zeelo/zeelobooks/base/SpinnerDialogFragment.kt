package com.zeelo.zeelobooks.base

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.zeelo.zeelobooks.R

class SpinnerDialogFragment : DialogFragment() {

    private val spinnerResource: String = "animations/loading_book.json"

    companion object {
        fun newInstance(): SpinnerDialogFragment {
            val fragment = SpinnerDialogFragment()
            return fragment
        }
    }

    //override fun getTheme(): Int = R.style.ZeeloTrasnparentDialog



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.spinner_dialog, container, false)
        setCancelable(false)
        return rootView
    }



    override fun dismiss() {
        this.dismissAllowingStateLoss()
    }

    override fun show(manager: FragmentManager, tag: String) {
        if (!this.isAdded) {
            try {
                manager.beginTransaction().add(this, tag).commitAllowingStateLoss()
            } catch (var4: Exception) {
                var4.printStackTrace()
            }

        }

    }
}
