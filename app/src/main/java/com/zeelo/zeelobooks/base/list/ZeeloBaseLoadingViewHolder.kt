package com.bet4gamers.app.base

import android.view.View

abstract class ZeeloBaseLoadingViewHolder(itemView: View) : ZeeloBaseViewHolder(itemView) {




    abstract fun bind()

    override fun getType(): Int {
        return LOADING_ITEM
    }


}