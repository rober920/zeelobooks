package com.zeelo.zeelobooks.features.books.booklist.model.entitys


class Book private constructor() {

    lateinit var link: String
        private set

    var id: Int = 0
        private set

    lateinit var title: String
        private set


    constructor(builder: Builder) : this() {
        this.link = builder.link
        this.id = builder.id
        this.title = builder.title

    }

    class Builder {

        var link: String = ""
            private set

        var id: Int = 0
            private set

        var title: String = ""
            private set

        fun setBookLink(link: String) =
                apply { this.link = link }


        fun setBookId(id: Int) =
                apply { this.id = id }


        fun setBookTitle(title: String) =
                apply { this.title = title }


        fun build(): Book =
                Book(this)


    }
}

