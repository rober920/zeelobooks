package com.zeelo.zeelobooks.features.books.createbook.views

import android.content.Intent
import com.zeelo.zeelobooks.R
import com.zeelo.zeelobooks.base.ZeeloBaseActivity
import com.zeelo.zeelobooks.features.books.createbook.presenter.CreateBookPresenter
import com.zeelo.zeelobooks.features.books.createbook.presenter.impl.CreateBookPresenterImpl
import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail
import kotlinx.android.synthetic.main.activity_create_book.*
import org.koin.android.ext.android.inject

class CreateBookActivity : ZeeloBaseActivity(), CreateBookPresenter.CreateBookView {

  private val mPresenter: CreateBookPresenterImpl by inject()

  companion object {

    fun startCreateBookActivity(activity: ZeeloBaseActivity) {
      val intent: Intent = Intent(activity, CreateBookActivity::class.java)
      activity.startActivity(intent)
    }
  }

  override fun intViews() {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setHomeButtonEnabled(true)

    create_book_button?.setOnClickListener { mPresenter.createBook() }
  }


  override fun getData() {
  }

  override fun decorateViews() {
    create_book_button?.text = getString(R.string.zeelo_createbook_create)
    create_book_title?.hint = getString(R.string.zeelo_createbook_title_hint)
    create_book_author?.hint = getString(R.string.zeelo_createbook_author_hint)
    create_book_price?.hint = getString(R.string.zeelo_createbook_price_hint)

  }

  override fun getLayout(): Int = R.layout.activity_create_book

  override fun initPresenter() {
    mPresenter.mView = this
  }

  override fun deletePresenter() {
    mPresenter.onDestroy()
  }

  override fun getBookTitle(): String? = create_book_title?.text.toString()

  override fun getBookPrice(): Double? = create_book_price?.text.toString().toDoubleOrNull()

  override fun getBookAuthor(): String? = create_book_author?.text.toString()

  override fun onBookCreated(result: BookDetail) {
    showMessage(getString(R.string.zeelo_createbook_create_OK))
  }

  override fun showTitleError() {
    create_book_title?.error = getString(R.string.zeelo_createbook_field_error)
  }

  override fun showPriceError() {
    create_book_price?.error = getString(R.string.zeelo_createbook_field_error)
  }

  override fun showAuthorError() {
    create_book_author?.error = getString(R.string.zeelo_createbook_field_error)
  }
}
