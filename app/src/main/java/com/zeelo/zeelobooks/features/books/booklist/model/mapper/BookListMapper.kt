package com.zeelo.zeelobooks.features.books.booklist.model.mapper

import com.zeelo.zeelobooks.features.books.booklist.model.entitys.Book
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.BookList
import com.zeelo.zeelobooks.features.books.repository.Mapper
import com.zeelo.zeelobooks.features.books.repository.models.ApiBook
import com.zeelo.zeelobooks.features.books.repository.models.ApiGetBookListResponse

class BookListMapper : Mapper<BookList, ApiGetBookListResponse> {


    override fun fromApi(apiModel: ApiGetBookListResponse): BookList {

        var listBuilder: BookList.Builder = BookList.Builder()

        for (apiBook: ApiBook in apiModel.books) {
            var bookBuilder: Book.Builder = Book.Builder()
            bookBuilder.setBookId(apiBook.id).setBookLink(apiBook.link).setBookTitle(apiBook.title)

            listBuilder.addBokk(bookBuilder.build())
        }

        return listBuilder.build()

    }

    override fun toApi(model: BookList): ApiGetBookListResponse {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}