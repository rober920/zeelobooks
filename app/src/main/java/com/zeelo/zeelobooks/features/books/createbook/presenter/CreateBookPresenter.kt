package com.zeelo.zeelobooks.features.books.createbook.presenter

import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail

interface CreateBookPresenter {

  fun createBook()

  interface CreateBookView : ZeeloBasePresenter.BaseView {

    fun getBookTitle(): String?
    fun getBookPrice(): Double?
    fun getBookAuthor(): String?
    fun onBookCreated(result: BookDetail)
    fun showTitleError()
    fun showPriceError()
    fun showAuthorError()
  }
}