package com.zeelo.zeelobooks.features.books.bookdetail.model.interactors

import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail
import com.zeelo.zeelobooks.features.books.bookdetail.model.mapper.BookDetailMapper
import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import io.reactivex.Observable

class GetBookDetailInteractor(private var repository: BooksRepository, private var mapper: BookDetailMapper) {

  fun getBookDetail(id: Int): Observable<BookDetail> {
    return repository.getBookDetail(id).map { book -> mapper.fromApi(book) }
  }
}