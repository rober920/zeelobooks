package com.zeelo.zeelobooks.features.books.bookdetail.views

import android.content.Intent
import com.zeelo.zeelobooks.R
import com.zeelo.zeelobooks.base.ZeeloBaseActivity
import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail
import com.zeelo.zeelobooks.features.books.bookdetail.presenter.BookDetailPresenter
import com.zeelo.zeelobooks.features.books.bookdetail.presenter.impl.BookDetailPresenterImpl
import kotlinx.android.synthetic.main.activity_book_detail.*
import org.koin.android.ext.android.inject

class BookDetailActivity : ZeeloBaseActivity(), BookDetailPresenter.BookDetailView {


    private val mPresenter: BookDetailPresenterImpl by inject()


    companion object {
        const val BOOK_ID: String = "book_id"

        fun startDetailActivity(activity: ZeeloBaseActivity, id: Int) {
            val intent: Intent = Intent(activity, BookDetailActivity::class.java)
            intent.putExtra(BOOK_ID, id)
            activity.startActivity(intent)
        }
    }

    override fun intViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }


    override fun getData() {

        if(intent!= null && intent.hasExtra(BOOK_ID)) {
            mPresenter.getBookDetail(intent.getIntExtra(BOOK_ID,-1))
        }
    }

    override fun decorateViews() {
    }

    override fun getLayout(): Int = R.layout.activity_book_detail

    override fun initPresenter() {
        mPresenter.mView = this
    }

    override fun deletePresenter() {
        mPresenter.onDestroy()
    }

    override fun onBookLoaded(result: BookDetail) {

        book_detail_title?.text = String.format(getString(R.string.zeelo_bookdetail_title),result.title)
        book_detail_price?.text = String.format(getString(R.string.zeelo_bookdetail_price),result.price)
        book_detail_author?.text = String.format(getString(R.string.zeelo_bookdetail_author),result.author)
    }


}
