package com.zeelo.zeelobooks.features.books.repository.models

import com.google.gson.annotations.SerializedName


data class ApiGetBookListResponse(

        @field:SerializedName("books")
        val books: ArrayList<ApiBook>
)