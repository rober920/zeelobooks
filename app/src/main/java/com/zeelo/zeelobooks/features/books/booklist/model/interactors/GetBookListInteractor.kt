package com.zeelo.zeelobooks.features.books.booklist.model.interactors

import com.zeelo.zeelobooks.features.books.booklist.model.entitys.BookList
import com.zeelo.zeelobooks.features.books.booklist.model.mapper.BookListMapper
import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import io.reactivex.Observable

class GetBookListInteractor(private var repository: BooksRepository, private var mapper: BookListMapper) {

  fun getBookList(offset: Int, count: Int): Observable<BookList> {
    return repository.getBookList(offset, count).map { books -> mapper.fromApi(books) }
  }
}