package com.zeelo.zeelobooks.features.books.bookdetail.model.entitys

class BookDetail private constructor() {


    var image: String? = null
    var author: String? = null
    var price: Double? = null
    var id: Int? = null
    var title: String? = null


    constructor(builder: BookDetail.Builder) : this() {

        this.image = builder.image
        this.author = builder.author
        this.price = builder.price
        this.id = builder.id
        this.title = builder.title
    }

    class Builder {

        var image: String? = null
            private set
        var author: String? = null
            private set
        var price: Double? = null
            private set
        var id: Int? = null
            private set
        var title: String? = null
            private set


        fun setBookImage(image: String?) = apply { this.image = image }
        fun setBookAuthor(author: String?) = apply { this.author = author }
        fun setBookPrice(price: Double?) = apply { this.price = price }
        fun setBookId(id: Int?) = apply { this.id = id }
        fun setBookTitle(title: String?) = apply { this.title = title }

        fun build(): BookDetail = BookDetail(this)

    }


}