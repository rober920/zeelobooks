package com.zeelo.zeelobooks.features.books

import android.app.Application
import com.zeelo.zeelobooks.koin.interactorModuleList
import com.zeelo.zeelobooks.koin.presenterModuleList
import com.zeelo.zeelobooks.koin.repositoryModuleList
import org.koin.android.ext.android.startKoin

class ZeeloApp : Application() {
    override fun onCreate() {
        super.onCreate()

        // Start Koin
        startKoin(this, listOf(presenterModuleList, interactorModuleList, repositoryModuleList))
    }
}