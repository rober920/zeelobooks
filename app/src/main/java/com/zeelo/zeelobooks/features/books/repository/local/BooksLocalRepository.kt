package com.zeelo.zeelobooks.features.books.repository.local

import android.app.Application
import com.google.gson.Gson
import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import com.zeelo.zeelobooks.features.books.repository.models.ApiBookDetail
import com.zeelo.zeelobooks.features.books.repository.models.ApiCreateBookRequest
import com.zeelo.zeelobooks.features.books.repository.models.ApiGetBookListResponse
import io.reactivex.Observable

class BooksLocalRepository(val application: Application) : BooksRepository {


  override fun getBookList(offset: Int?, count: Int?): Observable<ApiGetBookListResponse> {


    return Observable.create<ApiGetBookListResponse> { emitter ->
      try {
        val file: String = AndroidJsonReader.readJsonFile(application, "bookList.json")

        emitter.onNext(Gson().fromJson(file, ApiGetBookListResponse::class.java))
        emitter.onComplete()
      } catch (ex: Exception) {
        emitter.onError(ex)
      }

    }

  }

  override fun getBookDetail(id: Int): Observable<ApiBookDetail> {


    return Observable.create<ApiBookDetail> { emitter ->

      try {
        val file: String = AndroidJsonReader.readJsonFile(application, "bookDetail.json")

        emitter.onNext(Gson().fromJson(file, ApiBookDetail::class.java))
        emitter.onComplete()

      } catch (ex: Exception) {
        emitter.onError(ex)
      }

    }

  }

  override fun createBook(data: ApiCreateBookRequest): Observable<ApiBookDetail> {

    return Observable.create<ApiBookDetail> { emitter ->

      try {
        val file: String = AndroidJsonReader.readJsonFile(application, "bookDetail.json")

        emitter.onNext(Gson().fromJson(file, ApiBookDetail::class.java))
        emitter.onComplete()

      } catch (ex: Exception) {
        emitter.onError(ex)
      }

    }
  }
}