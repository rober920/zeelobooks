package com.zeelo.zeelobooks.features.books.booklist.views.adapter

import android.view.View
import com.zeelo.zeelobooks.R
import com.zeelo.zeelobooks.base.list.ZeeloBaseAdapter
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.Book


class BookListAdapter(listener: ZeeloBaseAdapter.BetBaseAdapterListener<Book>) : ZeeloBaseAdapter<Book, BookListViewHolder, BookListViewLoader>(listener) {


    override fun getViewLayout(): Int =
            R.layout.item_book


    override fun getLoadingView():
            Int = R.layout.item_book_loading


    override fun createItemViewHolder(view: View): BookListViewHolder =
            BookListViewHolder(view)


    override fun createLoadingViewHolder(view: View): BookListViewLoader =
            BookListViewLoader(view)
}