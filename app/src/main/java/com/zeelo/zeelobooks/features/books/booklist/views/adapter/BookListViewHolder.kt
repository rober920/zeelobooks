package com.zeelo.zeelobooks.features.books.booklist.views.adapter

import android.support.v7.widget.AppCompatTextView
import android.view.View
import com.bet4gamers.app.base.ZeeloBaseItemViewHolder
import com.zeelo.zeelobooks.R
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.Book

class BookListViewHolder(view: View) : ZeeloBaseItemViewHolder<Book>(view) {


    override fun bind(item: Book) {

        itemView.findViewById<AppCompatTextView>(R.id.book_item_title)?.text = item.title
    }
}