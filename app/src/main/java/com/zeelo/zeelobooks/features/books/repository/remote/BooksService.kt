package com.zeelo.zeelobooks.features.books.repository.remote

import com.zeelo.zeelobooks.features.books.repository.models.ApiBookDetail
import com.zeelo.zeelobooks.features.books.repository.models.ApiCreateBookRequest
import com.zeelo.zeelobooks.features.books.repository.models.ApiGetBookListResponse
import io.reactivex.Observable
import retrofit2.http.*

interface BooksService {


  @GET("api/v1/items")
  fun getBookList(@Query("offset") offset: Int?, @Query("count") count: Int?): Observable<ApiGetBookListResponse>

  @GET("api/v1/items/{id}")
  fun getBookDetail(@Path("id") id: Int): Observable<ApiBookDetail>

  @POST("api/v1/create")
  fun createBook(@Body bookRequest: ApiCreateBookRequest): Observable<ApiBookDetail>
}