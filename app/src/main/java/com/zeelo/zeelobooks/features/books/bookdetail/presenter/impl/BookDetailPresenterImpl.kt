package com.zeelo.zeelobooks.features.books.bookdetail.presenter.impl

import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.bookdetail.model.interactors.GetBookDetailInteractor
import com.zeelo.zeelobooks.features.books.bookdetail.presenter.BookDetailPresenter
import com.zeelo.zeelobooks.koin.inject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BookDetailPresenterImpl : ZeeloBasePresenter<BookDetailPresenter.BookDetailView>(), BookDetailPresenter {


    private val getBookDetailInteractor: GetBookDetailInteractor by inject()


    override fun getBookDetail(id: Int) {

        mView?.showLoading()
        getBookDetailInteractor.getBookDetail(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { result ->
                            mView?.hideLoading()
                            mView?.onBookLoaded(result)
                        }, { error ->
                    onServiceError(error)
                })
    }


}