package com.zeelo.zeelobooks.features.books.repository.models

import com.google.gson.annotations.SerializedName

data class ApiCreateBookRequest(

    @field:SerializedName("author")
    val author: String? = null,

    @field:SerializedName("price")
    val price: Double? = null,

    @field:SerializedName("title")
    val title: String? = null
)