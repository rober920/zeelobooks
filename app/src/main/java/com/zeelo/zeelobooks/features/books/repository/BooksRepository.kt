package com.zeelo.zeelobooks.features.books.repository

import com.zeelo.zeelobooks.features.books.repository.models.ApiBookDetail
import com.zeelo.zeelobooks.features.books.repository.models.ApiCreateBookRequest
import com.zeelo.zeelobooks.features.books.repository.models.ApiGetBookListResponse
import io.reactivex.Observable

interface BooksRepository {

  fun getBookList(offset: Int?, count: Int?): Observable<ApiGetBookListResponse>

  fun getBookDetail(id: Int): Observable<ApiBookDetail>

  fun createBook(data: ApiCreateBookRequest): Observable<ApiBookDetail>

}