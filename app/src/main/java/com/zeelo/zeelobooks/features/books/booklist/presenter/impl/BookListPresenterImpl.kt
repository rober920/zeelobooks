package com.zeelo.zeelobooks.features.books.booklist.presenter.impl

import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.booklist.model.interactors.GetBookListInteractor
import com.zeelo.zeelobooks.features.books.booklist.presenter.BookListPresenter
import com.zeelo.zeelobooks.koin.inject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BookListPresenterImpl : ZeeloBasePresenter<BookListPresenter.BookListView>(), BookListPresenter {


    private val getBookListInteractor: GetBookListInteractor by inject()

    override fun getBookList() {
        mView?.showLoading()
        getBookListInteractor.getBookList(10,20)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { result ->
                            mView?.hideLoading()
                            mView?.onBooksLoaded(result)
                        }, { error ->
                    onServiceError(error)
                })
    }

}