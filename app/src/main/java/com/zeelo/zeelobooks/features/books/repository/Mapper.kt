package com.zeelo.zeelobooks.features.books.repository

interface Mapper<M, A> {

    fun fromApi(apiModel: A): M

    fun toApi(model: M): A
}