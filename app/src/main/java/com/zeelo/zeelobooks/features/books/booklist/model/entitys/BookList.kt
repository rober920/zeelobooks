package com.zeelo.zeelobooks.features.books.booklist.model.entitys


class BookList private constructor() {

    lateinit var books: ArrayList<Book>
        private set

    constructor(builder: Builder) : this() {
        this.books = builder.books
    }

    class Builder {

        var books: ArrayList<Book> = ArrayList()
            private set

        fun addBokk(book: Book) =
                apply { books.add(book) }


        fun build(): BookList =
                BookList(this)


    }
}