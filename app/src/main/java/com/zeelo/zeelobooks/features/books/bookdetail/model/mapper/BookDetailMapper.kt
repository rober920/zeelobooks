package com.zeelo.zeelobooks.features.books.bookdetail.model.mapper

import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail
import com.zeelo.zeelobooks.features.books.repository.Mapper
import com.zeelo.zeelobooks.features.books.repository.models.ApiBookDetail

class BookDetailMapper : Mapper<BookDetail, ApiBookDetail> {


    override fun fromApi(apiModel: ApiBookDetail): BookDetail {

        var bookBuilder: BookDetail.Builder = BookDetail.Builder()


        bookBuilder.setBookId(apiModel.id)
                .setBookAuthor(apiModel.author)
                .setBookImage(apiModel.image)
                .setBookPrice(apiModel.price)
                .setBookTitle(apiModel.title)


        return bookBuilder.build()

    }

    override fun toApi(model: BookDetail): ApiBookDetail {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}