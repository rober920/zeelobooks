package com.zeelo.zeelobooks.features.books.booklist.presenter

import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.BookList

interface BookListPresenter {

    fun getBookList()

    interface BookListView : ZeeloBasePresenter.BaseView {
        fun onBooksLoaded(result: BookList)
    }
}