package com.zeelo.zeelobooks.features.books.createbook.presenter.impl

import android.text.TextUtils
import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.createbook.model.interactors.CreateBookInteractor
import com.zeelo.zeelobooks.features.books.createbook.presenter.CreateBookPresenter
import com.zeelo.zeelobooks.koin.inject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CreateBookPresenterImpl : ZeeloBasePresenter<CreateBookPresenter.CreateBookView>(), CreateBookPresenter {

  private val createBookInterface: CreateBookInteractor by inject()

  override fun createBook() {

    val title: String? = mView?.getBookTitle()
    val price: Double? = mView?.getBookPrice()
    val author: String? = mView?.getBookAuthor()


    if (TextUtils.isEmpty(title)) {
      mView?.showTitleError()
      return
    }

    if (price == null) {
      mView?.showPriceError()
      return
    }

    if (TextUtils.isEmpty(author)) {
      mView?.showAuthorError()
      return
    }

    mView?.showLoading()
    createBookInterface.createBook(title!!, price!!, author!!)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(
            { result ->
              mView?.hideLoading()
              mView?.onBookCreated(result)
            }, { error ->
          onServiceError(error)
        })


  }
}