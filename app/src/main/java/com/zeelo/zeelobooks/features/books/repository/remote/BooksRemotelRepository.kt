package com.zeelo.zeelobooks.features.books.repository.remote

import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import com.zeelo.zeelobooks.features.books.repository.models.ApiBookDetail
import com.zeelo.zeelobooks.features.books.repository.models.ApiCreateBookRequest
import com.zeelo.zeelobooks.features.books.repository.models.ApiGetBookListResponse
import io.reactivex.Observable

class BooksRemotelRepository() : BooksRepository {

  private val bookSevices: BooksService = BookServicesFactory.create()


  override fun getBookList(offset: Int?, count: Int?): Observable<ApiGetBookListResponse> = bookSevices.getBookList(offset, count)


  override fun getBookDetail(id: Int): Observable<ApiBookDetail> = bookSevices.getBookDetail(id)

  override fun createBook(data: ApiCreateBookRequest): Observable<ApiBookDetail> = bookSevices.createBook(data)
}