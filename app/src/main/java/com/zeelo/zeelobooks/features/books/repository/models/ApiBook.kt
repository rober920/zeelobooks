package com.zeelo.zeelobooks.features.books.repository.models

import com.google.gson.annotations.SerializedName

data class ApiBook(

        @field:SerializedName("link")
        val link: String,

        @field:SerializedName("id")
        val id: Int,

        @field:SerializedName("title")
        val title: String
)