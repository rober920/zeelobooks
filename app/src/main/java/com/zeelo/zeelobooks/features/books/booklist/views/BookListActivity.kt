package com.zeelo.zeelobooks.features.books.booklist.views

import android.support.v7.widget.LinearLayoutManager
import com.zeelo.zeelobooks.R
import com.zeelo.zeelobooks.base.ZeeloBaseActivity
import com.zeelo.zeelobooks.base.list.ZeeloBaseAdapter
import com.zeelo.zeelobooks.features.books.createbook.views.CreateBookActivity
import com.zeelo.zeelobooks.features.books.bookdetail.views.BookDetailActivity
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.Book
import com.zeelo.zeelobooks.features.books.booklist.model.entitys.BookList
import com.zeelo.zeelobooks.features.books.booklist.presenter.BookListPresenter
import com.zeelo.zeelobooks.features.books.booklist.presenter.impl.BookListPresenterImpl
import com.zeelo.zeelobooks.features.books.booklist.views.adapter.BookListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class BookListActivity : ZeeloBaseActivity(), BookListPresenter.BookListView, ZeeloBaseAdapter.BetBaseAdapterListener<Book> {


  private val mPresenter: BookListPresenterImpl by inject()
  private lateinit var mAdapter: BookListAdapter

  override fun intViews() {

    setSupportActionBar(toolbar)

    mAdapter = BookListAdapter(this)

    recyclerView.adapter = mAdapter
    recyclerView.layoutManager = LinearLayoutManager(this)

    fabutton.setOnClickListener { CreateBookActivity.startCreateBookActivity(this) }
  }


  override fun getData() {
    mPresenter.getBookList()
  }

  override fun decorateViews() {

  }

  override fun getLayout(): Int = R.layout.activity_main

  override fun initPresenter() {
    mPresenter.mView = this
  }

  override fun deletePresenter() {
    mPresenter.onDestroy()
  }

  override fun onBooksLoaded(result: BookList) {
    mAdapter.data = result.books
    mAdapter.moreData = false
  }


  override fun onClickItem(item: Book, position: Int) {

    BookDetailActivity.startDetailActivity(this, item.id)
  }

  override fun onLoadingShow() {
    mPresenter.getBookList()
  }


}
