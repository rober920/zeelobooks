package com.zeelo.zeelobooks.features.books.createbook.model.interactors

import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail
import com.zeelo.zeelobooks.features.books.bookdetail.model.mapper.BookDetailMapper
import com.zeelo.zeelobooks.features.books.repository.BooksRepository
import com.zeelo.zeelobooks.features.books.repository.models.ApiCreateBookRequest
import io.reactivex.Observable

class CreateBookInteractor(private var repository: BooksRepository, private var mapper: BookDetailMapper) {

  fun createBook(title: String, price: Double, author: String): Observable<BookDetail> {
    return repository.createBook(ApiCreateBookRequest(title, price, author)).map { book -> mapper.fromApi(book) }
  }
}