package com.zeelo.zeelobooks.features.books.bookdetail.presenter

import com.zeelo.zeelobooks.base.ZeeloBasePresenter
import com.zeelo.zeelobooks.features.books.bookdetail.model.entitys.BookDetail

interface BookDetailPresenter {

    fun getBookDetail(id: Int)

    interface BookDetailView : ZeeloBasePresenter.BaseView {
        fun onBookLoaded(result: BookDetail)
    }
}